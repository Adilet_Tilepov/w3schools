import sys
import matplotlib
matplotlib.use('Agg')
import pandas as pd
import matplotlib.pyplot as plt
data = pd.read_csv("data.csv", header=0, sep=",")
data.plot(x ='Продолжительность', y='Сожженные_калории', kind='scatter'),
plt.show()
plt.savefig(sys.stdout.buffer)
sys.stdout.flush()
