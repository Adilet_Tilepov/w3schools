#Three lines to make our compiler able to draw:
import sys
import matplotlib
matplotlib.use('Agg')
import pandas as pd
import matplotlib.pyplot as plt
Drowning_Accident = [30, 45, 50, 70, 90, 110, 130, 150, 170, 190]
Ice_Cream_Sale = [25, 35, 55, 75, 95, 115, 135, 155, 175, 195]
Drowning = {"Drowning Accident": [30, 45, 50, 70, 90, 110, 130, 150, 170, 190],
            "Ice_Cream_Sale": [25, 35, 55, 75, 95, 115, 135, 155, 175, 195]}
Drowning = pd. DataFrame(data=Drowning)
Drowning.plot(x="Ice_Cream_Sale", y="Drowning_Accident", kind="scatter")
plt.show()
correlation_beach = Drowning.corr()
print(correlation_beach)
#Two lines to make our compiler able to draw:
plt.savefig(sys.stdout.buffer)
sys.stdout.flush()
