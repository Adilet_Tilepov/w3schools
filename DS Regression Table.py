import pandas as pd
import statsmodels.formula.api as smf
data = pd.read_csv("data.csv", header=0, sep=",")
model = smf.ols('Calorie_Burnage ~ Average_Pulse', data = data)
results = model.fit()
print(results.summary())
