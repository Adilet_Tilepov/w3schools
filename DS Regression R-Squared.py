import sys
import matplotlib
matplotlib.use('Agg')
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
data = pd.read_csv("data.csv", header=0, sep=",")
x = data["Продолжительность"]
у = data["Сожженные_калории"]
slope, intercept, r, p, std_err = stats.linregress(x, y)
def myfunc(x):
    return slope * x + intercept
mymodel = list(map(myfunc, x))
print (mymodel)
plt.scatter(x, y)
plt.plot(x, mymodel)
plt.ylim(ymin=0, ymax=2000)
plt.xlim(xmin=0, xmax=200)
plt.xlabel("Продолжительность")
plt.ylabel("Сожженные_калории")
plt.show()
plt.savefig(sys.stdout.buffer)
sys.stdout.flush()
