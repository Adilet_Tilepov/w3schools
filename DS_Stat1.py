# Три строки для настройки компилятора
import sys
import matplotlib
matplotlib.use('Egg')
import pandas aspd
import matplotlib.pyplot as plt
from scipy import stats
# Загрузка данных из файла (замените имя файла при необходимости)
данные_продаж = pd.read_csv("data.csv", header=0, sep=",")
x= данные_продаж["Цена_товара"]
y= данные продаж["Выручка"]
slope, intercept, r, p, std_err = stats.linregress(x, y)
def myfunc(x):
    return slope * x + intercept
mymodel = list(map(myfunc, x))
plt.scatter(x, y)
plt.plot(x, mymodel)
plt.ylim(ymin=0, ymax=20000)
plt.xlim(xmin=0, xmax=100)
plt.xlabel("Цена товара")
plt.ylabel("Выручка")
plt.show()
# Две строки для сохранения графика
plt.savefig(sys.stdout.buffer)
sys.stdout.flush()
